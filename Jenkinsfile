pipeline {
    environment { 
        registry = "tjozsa/test-image-xx" 
        registryCredential = 'tjozsa_dockerhub' 
        dockerImage = '' 
    }

    agent none

    stages {

        stage('Clone') {
            agent any
            steps {
                cleanWs deleteDirs: true
                git credentialsId: 'devops_jenkins_ssh',
                    url: 'https://gitlab.com/tjozsa/04-jenkins-multibranch.git'
            }
        }

        stage('Build') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2'
                }
            }
            steps {
                sh 'mvn clean install'
            }
        }
    
        stage('Code quality check') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2 --network devops_ci_cd_cicd-demo-network'
                }
            }
            steps {
                withSonarQubeEnv('sonarqube') { 
                    sh 'mvn sonar:sonar -Dsonar.projectKey=my-first-project'
                }
            }
        }
        
        stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        
        stage('Package deploy') {
            agent {
                docker {
                    image 'maven:3-alpine'
                    args '-v $HOME/.m2:/root/.m2 --network devops_ci_cd_cicd-demo-network'
                }
            }
            steps {
                sh 'mvn --settings settings.xml deploy'
            }
        }

        stage('Docker build') {
            agent any
            steps {
                script {
                    def fileName = "demo-0.0.1-SNAPSHOT.jar"
                    // docker.build("test-image-xx", "--build-arg WAR_NAME=${fileName} .")
                    dockerImage = docker.build registry + ":$BUILD_NUMBER" , "--build-arg WAR_NAME=${fileName} ."
                }
                
            }
        }

        stage ('Approve to Deploy') {
            agent any
            steps {
                input message "Deploy?"
            }
        }

        stage ('Docker deploy') {
            agent any
            steps {
                script { 
                        docker.withRegistry( '', registryCredential ) { 
                            dockerImage = docker.tag 
                            dockerImage.push() 
                        }
                }
            }
        }
    }
}
